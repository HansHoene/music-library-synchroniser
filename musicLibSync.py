#/usr/bin/python

# Hans-Edward Hoene
# Copyright 2020

import sys
import os
import getopt
import shutil

# Brief: print help menu
def printHelp():
    print("");
    print("musicLibSync.py [OPTIONS] -i <INPUT M3U playlist> -o <OUTPUT DIRECTORY>");
    print("");
    print("Options:");
    print("    -d, --dry-run       Perform a dry run");
    print("    -h, --help          Print help menu and exit");
    print("    -b, --base=<arg>    Set base directory where all music files are located");
    print("    -i, --input=<arg>   Set input M3U playlist file");
    print("    -o, --output=<arg>  Set output directory");
    print("    -s, --simple        Simple M3U playlist (only a list of files)");
    print("");

# Read M3U playlist into array of files
def readM3UPlaylist(fileName, simple):
    files = [];

    # read entire file
    m3u = open(fileName, 'r');
    lines = m3u.read().splitlines();
    m3u.close();

    # check file size
    if (len(lines) < 1):
        print("Error: emtpy M3U file");
        sys.exit(-1);

    # loop through lines
    count = 0;
    for line in lines:
        if simple:
            files.append(line);
        else:
            # check line
            if (count == 0):
                # first line should match
                if (line != "#EXTM3U"):
                    print("M3U syntax error on line %d" % count);
                    sys.exit(-1);
            elif (count & 0x1):
                if (line[0:8] != "#EXTINF:"):
                    print("Syntax error in M3U on line %d" % count);
                    sys.exit(-1);
            else:
                files.append(line);
        count += 1;

    return files;

# process operations
def processOperations(files, base, outDir):
    ret = [];

    # correct base
    if (len(base) == 0):
        base = "/";
    elif (base[len(base) - 1] != "/"):
        base += "/";

    # correct out dir
    if (len(outDir) == 0):
        outDir = "/";
    elif (outDir[len(outDir) - 1] != "/"):
        outDir += "/";

    # loop through all files
    for file in files:
        # check if file matches base
        if (file[0:len(base)] != base):
            print("Error: file does not match expected base");
            print("    File = \"%s\"" % file);
            print("    Base = \"%s\"" % base);
            sys.exit(-1);

        # get output file name
        outFileName = outDir + file[len(base):];
        outDirName = os.path.dirname(outFileName);

        # output
        ret.append([
            file,
            outDirName,
            outFileName
        ]);

    return ret;

# sync
def executeOperations(ops, dryRun):
    count = 0;
    for file, outDir, outFile in ops:
        if (dryRun):
            if (not os.path.isdir(outDir)):
                print("mkdir \"%s\"" % outDir);
            if (not os.path.isfile(outFile)):
                print("copy \"%s\" \"%s\"" % (file, outFile));
        else:
            if (not os.path.isdir(outDir)):
                os.makedirs(outDir);
            if (not os.path.isfile(outFile)):
                shutil.copy2(file, outFile);
            count += 1;
            print("\rCopied %d of %d files..." % (count, len(ops)), end='          ');

# Main function
def main(argv):
    # process arguments
    dryRun = 0;
    base = "";
    playlist = "";
    outDir = "";
    simple=False;

    try:
        opts, args = getopt.getopt(argv, "hdb:i:o:s", ["dry-run", "help", "base=", "input=", "output=", "simple"]);
    except getopt.GetoptError as err:
        # print help information and exit
        print(err);
        printHelp();
        return (-1);
    for opt, arg in opts:
        if (opt in ("-h", "--help")):
            printHelp();
            return 0;
        elif (opt in ("-d", "--dry-run")):
            dryRun = 1;
        elif (opt in ("-b", "--base")):
            base = arg;
        elif (opt in ("-i", "--input")):
            playlist = arg;
        elif (opt in ("-o", "--output")):
            outDir = arg;
        elif (opt in ("-s", "--simple")):
            simple=True;
        else:
            print("Error: unahandled option '%s'" % opt);
            printHelp();
            return (-1);

    # missing args?
    if (playlist == ""):
        print("Syntax Error: Missing playlist argument");
        printHelp();
        return (-1);
    if (outDir == ""):
        print("Syntax Error: Missing outptu directory argument");
        printHelp();
        return (-1);

    # print info
    print("M3U Playlist = \"%s\"" % playlist);
    print("Output Directory = \"%s\"" % outDir);
    if (base != ""):
        print("Base Directory = \"%s\"" % base);
    if (dryRun):
        print("Dry run flag is on");
    if (simple):
        print("Simple playlist flag is on");
    print("");

    # process M3U into file list
    files = readM3UPlaylist(playlist, simple);

    # process into operations
    ops = processOperations(files, base, outDir);

    # execute operations
    executeOperations(ops, dryRun);

    return 0;

# run main function
if (__name__ == "__main__"):
    # execute only if run as a script
    sys.exit(main(sys.argv[1:]));
